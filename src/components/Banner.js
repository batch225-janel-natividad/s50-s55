// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

//Destructure importation
import {Button, Row, Col} from 'react-bootstrap';

export default function Banner() {
    return(
        <Row>
            <Col>
                <h1> Zuitt Coding Bootcamp</h1>
                <p>Oppurtunities for everyone, everywhere
                </p>
                <Button variant="primary">Ennrol now</Button>
            </Col>
        </Row>
    )
}