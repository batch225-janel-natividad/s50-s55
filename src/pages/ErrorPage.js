import { Link } from 'react-router-dom';

export default function Error() {

    return (
        <>
            <div>
              <h1>404: Page Not Found</h1>
              <p>Go Back to <Link to="/">homepage.</Link></p>
            </div>
         </>
          );
    
}