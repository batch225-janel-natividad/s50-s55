import React from 'react';

//Create a Context object

//Context object is a different approach in passing infomration between components and allows easier access by avoiding props-drilling.

//type of object that can be used to store information that can be shared to other components within the app
const UserContext = React.createContext() // Base

// The " Provider" component allows other components to consume /use the context object and supply the necessary information need to the context object

export const UserProvider = UserContext.Provider; //Deliver

export default UserContext